import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Button, Dimensions } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { LineChart } from 'react-native-chart-kit';
import { useSelector } from 'react-redux';

const StatisticsScreen = () => {
  const weightArray = useSelector(state => state.weightArray);
  const [datesArray, setDatesArray] = useState([]);
  const [weightsArray, setWeightsArray] = useState([]);

  useEffect(() => {
    const dateArr = [];
    const weightArr = [];
    weightArray.forEach(item => {
      dateArr.push(
        `${item.date.day < 9 ? `0${item.date.day}` : item.date.day}.${
          item.date.month < 9 ? `0${item.date.month}` : item.date.month
        }`
      );
      weightArr.push(item.weight);
    });
    setDatesArray(dateArr);
    setWeightsArray(weightArr);
  }, [weightArray]);

  const renderChart = () => {
    if (datesArray.length !== 0) {
      return (
        <LineChart
          data={{
            labels: datesArray,
            datasets: [
              {
                data: weightsArray,
              },
            ],
          }}
          width={Dimensions.get('window').width}
          height={240}
          chartConfig={{
            backgroundGradientFrom: '#fafafa',
            backgroundGradientTo: '#fafafa',
            decimalPlaces: 0,
            color: (opacity = 255) => `rgba(64, 84, 251, ${opacity})`,
            style: {
              borderRadius: 16,
            },
          }}
          bezier
          style={styles.chart}
        />
      );
    } else {
      return <Text>No data to display</Text>;
    }
  };

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.title}>Weight loss progress</Text>
      {renderChart()}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    marginVertical: 20,
  },
  chart: {
    marginVertical: 8,
    marginLeft: -16,
  },
});

export default StatisticsScreen;
