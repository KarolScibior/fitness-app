import React from 'react';
import { StyleSheet, Button } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { useSelector } from 'react-redux';
import HeightSetter from '../components/AboutScreen/HeightSetter';
import WeightSetter from '../components/AboutScreen/WeightSetter';
import BMI from '../components/AboutScreen/BMI';
import DaysTrained from '../components/AboutScreen/DaysTrained';
import AgeSetter from '../components/AboutScreen/AgeSetter';
import Divider from '../components/Divider';
import { cancelAllNotifications } from '../notifications/Notifications';
import NotificationSetter from '../components/AboutScreen/NotificationSetter';

const AboutScreen = () => {
  const data = useSelector(state => state.beginnerTraining);
  const logA = () => {
    console.log(data);
    //scheduleNotification();
    //cancelAllNotifications();
  };

  return (
    <ScrollView style={styles.container}>
      <HeightSetter />
      <Divider />
      <WeightSetter />
      <Divider />
      <AgeSetter />
      <Divider />
      <BMI />
      <Divider />
      <DaysTrained />
      <Divider />
      <NotificationSetter />
      <Button title='elo' onPress={logA} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
  },
});

export default AboutScreen;
