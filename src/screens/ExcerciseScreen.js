import React, { useState } from 'react';
import NoExcercise from '../components/ExcerciseScreen/NoExcercise';
import FinishExcercising from '../components/ExcerciseScreen/FinishExcercising';
import TimeExcercise from '../components/ExcerciseScreen/TimeExcercise';
import RestTime from '../components/ExcerciseScreen/RestTime';
import RepeatExcercise from '../components/ExcerciseScreen/RepeatExcercise';

const ExcerciseScreen = ({ navigation, route }) => {
  const [currentExcercise, setCurrentExcercise] = useState(0);
  const [rest, setRest] = useState(false);

  const nextExcercise = () => {
    setTimeout(() => {
      setCurrentExcercise(currentExcercise + 1);
      setRest(false);
    }, 1000);
  };

  const setBreak = () => {
    setTimeout(() => {
      setRest(true);
    }, 1000);
  };

  if (currentExcercise === 0) {
    return (
      <NoExcercise
        setCurrentExcercise={setCurrentExcercise}
        navigation={navigation}
      />
    );
  } else if (currentExcercise === route.params.excercises.length + 1) {
    return <FinishExcercising navigation={navigation} item={route.params} />;
  } else if (
    route.params.excercises[currentExcercise - 1].time !== undefined &&
    !rest
  ) {
    return (
      <TimeExcercise
        route={route}
        currentExcercise={currentExcercise}
        setBreak={setBreak}
      />
    );
  } else if (rest) {
    return <RestTime nextExcercise={nextExcercise} />;
  } else {
    return (
      <RepeatExcercise
        currentExcercise={currentExcercise}
        route={route}
        setCurrentExcercise={setCurrentExcercise}
      />
    );
  }
};

export default ExcerciseScreen;
