import React from 'react';
import { StyleSheet, Text, View, FlatList, Button } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import ExcerciseListItem from '../components/ModalScreen/ExcerciseListItem';
import Colors from '../constants/Colors';
import { useSelector } from 'react-redux';
import Divider from '../components/Divider';

const ModalScreen = ({ navigation }) => {
  const item = useSelector(state => state.currentDay);

  if (item.description !== 'Break') {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.dayText}>Day {item.day}</Text>
          <Text style={styles.levelText}>{item.level}</Text>
        </View>
        <FlatList
          ItemSeparatorComponent={Divider}
          data={item.excercises}
          renderItem={({ item }) => <ExcerciseListItem item={item} />}
          keyExtractor={item => item.id.toString()}
        />
        <View style={styles.footer}>
          <Button
            onPress={() => navigation.navigate('Training', item)}
            title={item.completed ? 'Already completed' : 'Begin'}
            disabled={item.completed}
            color={Colors.tintColor}
          />
        </View>
      </View>
    );
  } else {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.dayText}>Day {item.day}</Text>
          <Text style={styles.levelText}>{item.level}</Text>
        </View>
        <View style={styles.breakContainer}>
          <Ionicons name='ios-bed' size={120} color={Colors.tintColor} />
          <Text style={styles.restText}>Let your body rest today...</Text>
        </View>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
  },
  header: {
    padding: 8,
    alignItems: 'center',
  },
  footer: {
    padding: 8,
  },
  dayText: {
    fontSize: 32,
  },
  levelText: {
    fontSize: 16,
    color: 'grey',
  },
  breakContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  restText: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 64,
  },
});

export default ModalScreen;
