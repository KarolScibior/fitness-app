import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Colors from '../../constants/Colors';
import DayItem from './DayItem';

const CarouselItem = ({ navigation, item }) => {
  const renderDays = days => {
    return days.days.map((item, index) => (
      <DayItem
        item={item}
        key={index}
        level={days.title}
        navigation={navigation}
      />
    ));
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <View style={styles.titleContainer}>
          <Text style={styles.headerTitle}>{item.title}</Text>
          <View style={styles.iconsContainer}>
            <Ionicons
              name='ios-fitness'
              size={20}
              style={styles.levelIcon}
              color={
                item.level >= 1 ? Colors.tabIconSelected : Colors.tabIconDefault
              }
            />
            <Ionicons
              name='ios-fitness'
              size={20}
              style={styles.levelIcon}
              color={
                item.level >= 2 ? Colors.tabIconSelected : Colors.tabIconDefault
              }
            />
            <Ionicons
              name='ios-fitness'
              size={20}
              style={styles.levelIcon}
              color={
                item.level >= 3 ? Colors.tabIconSelected : Colors.tabIconDefault
              }
            />
          </View>
        </View>
        <Text>{item.description}</Text>
      </View>
      <View style={styles.daysContainer}>{renderDays(item)}</View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    margin: 16,
  },
  headerContainer: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    backgroundColor: '#fafafa',
    padding: 8,
    borderRadius: 16,
    marginBottom: 20,
  },
  titleContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  iconsContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  levelIcon: {
    margin: 4,
  },
  headerTitle: {
    fontSize: 24,
  },
  daysContainer: {
    width: '100%',
    alignItems: 'stretch',
  },
});

export default CarouselItem;
