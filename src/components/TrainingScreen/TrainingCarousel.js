import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import CarouselItem from './CarouselItem';
import * as Layout from '../../constants/Layout';
import { useSelector } from 'react-redux';

const TrainingCarousel = ({ navigation }) => {
  const [activeSlide, setActiveSlide] = useState(0);
  const [entries, setEntries] = useState([]);

  const beginnerTraining = useSelector(state => state.beginnerTraining);
  const intermediateTraining = useSelector(state => state.intermediateTraining);
  const advancedTraining = useSelector(state => state.advancedTraining);

  useEffect(() => {
    setEntries([beginnerTraining, intermediateTraining, advancedTraining]);
  }, [beginnerTraining, intermediateTraining, advancedTraining]);

  const renderItem = ({ item, index }) => {
    return <CarouselItem item={item} index={index} navigation={navigation} />;
  };

  return (
    <View>
      <Carousel
        layout={'default'}
        data={entries}
        renderItem={renderItem}
        sliderWidth={Layout.default.window.width}
        itemWidth={Layout.default.window.width}
        onSnapToItem={index => setActiveSlide(index)}
      />
      <Pagination dotsLength={entries.length} activeDotIndex={activeSlide} />
    </View>
  );
};

export default TrainingCarousel;
