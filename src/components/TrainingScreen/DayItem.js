import React, { useState } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  CheckBox,
} from 'react-native';
import { useDispatch } from 'react-redux';
import { actions } from '../../redux/ducks';

const DayItem = ({ navigation, item, level }) => {
  const dispatch = useDispatch();

  const setDay = (day, level) =>
    dispatch(actions.setCurrentDay({ ...day, level }));

  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate('Modal');
        setDay(item, level);
      }}
    >
      <View style={styles.dayContainer}>
        <View style={styles.textContainer}>
          <Text style={styles.dayTitle}>Day {item.day}</Text>
          <Text style={styles.dayDescription}>{item.description}</Text>
        </View>
        <CheckBox
          value={item.description === 'Break' ? true : item.completed}
          disabled
        />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  daysContainer: {
    width: '100%',
    alignItems: 'stretch',
  },
  dayContainer: {
    padding: 10,
    margin: 4,
    backgroundColor: '#fafafa',
    borderRadius: 4,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textContainer: {
    flexDirection: 'column',
  },
  dayTitle: {
    fontSize: 20,
  },
  dayDescription: {
    fontSize: 12,
    color: '#ccc',
  },
});

export default DayItem;
