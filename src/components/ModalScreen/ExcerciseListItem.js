import React, { useState } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Modal,
  Image,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Colors from '../../constants/Colors';

const ExcerciseListItem = ({ item }) => {
  const [modalVisible, setModalVisible] = useState(false);

  const renderSecondaryText = excercise => {
    if (excercise.time === undefined) {
      return `x${excercise.repeats}`;
    } else {
      return `00:${excercise.time}`;
    }
  };

  return (
    <>
      <TouchableOpacity
        onPress={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.container}>
          <Image
            style={styles.listGif}
            source={{
              uri:
                'https://media.tenor.com/images/b3c6af49c64c7dd23caca7f0b2185107/tenor.gif',
            }}
          />
          <View style={styles.textContainer}>
            <Text style={styles.excerciseName}>{item.name}</Text>
            <Text style={styles.excerciseDescription}>
              {renderSecondaryText(item)}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
      <Modal animationType='slide' transparent={true} visible={modalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={styles.xContainer}>
              <TouchableOpacity
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}
              >
                <Text style={styles.closeButon}>×</Text>
              </TouchableOpacity>
            </View>
            <Text style={styles.excerciseNameModal}>{item.name}</Text>
            <Image
              style={styles.gif}
              source={{
                uri:
                  'https://media.tenor.com/images/b3c6af49c64c7dd23caca7f0b2185107/tenor.gif',
              }}
            />
            <Text style={styles.excerciseDescriptionModal}>
              {item.description}
            </Text>
          </View>
        </View>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 8,
    marginLeft: 8,
    marginRight: 8,
    marginBottom: 4,
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  excerciseName: {
    textAlign: 'right',
    fontSize: 16,
    fontWeight: 'bold',
  },
  excerciseDescription: {
    textAlign: 'right',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    width: '80%',
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  xContainer: {
    width: '100%',
    paddingRight: 8,
    alignItems: 'flex-end',
  },
  closeButon: {
    fontSize: 24,
  },
  excerciseNameModal: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 16,
    textAlign: 'center',
  },
  excerciseDescriptionModal: {
    color: 'grey',
    textAlign: 'justify',
    padding: 8,
    marginTop: 8,
  },
  gif: {
    width: 220,
    height: 167,
  },
  listGif: {
    width: 55,
    height: 40,
  },
});

export default ExcerciseListItem;
