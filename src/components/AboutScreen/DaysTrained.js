import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { useSelector } from 'react-redux';

const DaysTrained = () => {
  const days = useSelector(state => state.daysTrained);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Days Trained</Text>
      <Text style={styles.title}>{days}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 16,
    marginBottom: 16,
    paddingLeft: 16,
    paddingRight: 16,
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
  },
});

export default DaysTrained;
