import React from 'react';
import { View, Text, StyleSheet, Switch } from 'react-native';
import {
  scheduleNotification,
  cancelAllNotifications,
} from '../../notifications/Notifications';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../../redux/ducks';
import Colors from '../../constants/Colors';

const NotificationSetter = () => {
  const dispatch = useDispatch();
  const notifications = useSelector(state => state.notifications);
  const setNotifications = () => dispatch(actions.setNotifications());

  const toggleSwitch = () => {
    if (notifications === false) {
      scheduleNotification();
    } else {
      cancelAllNotifications();
    }
    setNotifications();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Notifications</Text>
      <Switch
        value={notifications}
        onValueChange={toggleSwitch}
        trackColor={{ true: Colors.tintColor }}
        thumbColor='white'
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 16,
    marginBottom: 16,
    paddingLeft: 16,
    paddingRight: 16,
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
  },
});

export default NotificationSetter;
