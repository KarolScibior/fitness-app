import React, { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Modal,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { useSelector, useDispatch } from 'react-redux';
import { actions } from '../../redux/ducks';

const HeightSetter = () => {
  const dispatch = useDispatch();
  const storeHeight = useSelector(state => state.height);
  const setStoreHeight = height => dispatch(actions.setHeight(height));
  const [height, setHeight] = useState(storeHeight.toString());
  const [modalVisible, setModalVisible] = useState(false);

  const sumbitHeight = () => {
    if (Number.isInteger(Number(height)) && Number(height) > 0) {
      setStoreHeight(Number(height));
      setModalVisible(!modalVisible);
    } else {
      ToastAndroid.show('Incorrect input!', ToastAndroid.SHORT);
    }
  };

  return (
    <>
      <TouchableOpacity onPress={() => setModalVisible(!modalVisible)}>
        <View style={styles.container}>
          <Text style={styles.title}>Height</Text>
          <Text style={styles.title}>{storeHeight}</Text>
        </View>
      </TouchableOpacity>
      <Modal animationType='slide' transparent={true} visible={modalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={styles.xContainer}>
              <TouchableOpacity onPress={() => setModalVisible(!modalVisible)}>
                <Text style={styles.closeButon}>×</Text>
              </TouchableOpacity>
            </View>
            <Text style={styles.title}>Height</Text>
            <TextInput
              keyboardType='numeric'
              placeholder='cm'
              style={styles.input}
              value={height}
              onChangeText={text => setHeight(text)}
              onSubmitEditing={sumbitHeight}
            />
          </View>
        </View>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 16,
    marginBottom: 16,
    paddingLeft: 16,
    paddingRight: 16,
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
  },
  input: {
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    margin: 24,
    width: '20%',
    fontSize: 20,
    textAlign: 'right',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    width: '80%',
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  xContainer: {
    width: '100%',
    paddingRight: 8,
    alignItems: 'flex-end',
  },
  closeButon: {
    fontSize: 32,
  },
});

export default HeightSetter;
