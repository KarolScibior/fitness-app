import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Modal, TouchableOpacity } from 'react-native';
import { useSelector } from 'react-redux';

const BMI = () => {
  const [bmi, setBmi] = useState(0);
  const [modalVisible, setModalVisible] = useState(false);

  const height = useSelector(state => state.height);
  const weight = useSelector(state => state.weight);
  const age = useSelector(state => state.age);

  const calculateBMI = (height, weight) => {
    if (height > 0 && weight > 0) {
      const heightM = height / 100;
      return weight / (heightM * heightM);
    }
  };

  const renderBMI = (bmi, age) => {
    if (bmi > 0 && age > 0) {
      let okay = false;
      let text = '';
      if (
        (age >= 19 && age <= 24 && bmi >= 19 && bmi <= 24) ||
        (age <= 34 && bmi >= 20 && bmi <= 25) ||
        (age <= 44 && bmi >= 21 && bmi <= 26) ||
        (age <= 54 && bmi >= 22 && bmi <= 27) ||
        (age <= 64 && bmi >= 23 && bmi <= 28) ||
        (age > 64 && bmi >= 24 && bmi <= 29)
      ) {
        okay = true;
      }
      if (bmi < 15) {
        text = 'You are very severely underweight.';
      } else if (bmi < 16) {
        text = 'You are severely underweight.';
      } else if (bmi < 18.5) {
        text = 'You are underweight.';
      } else if (bmi < 26) {
        text = 'Your weight is healthy.';
      } else if (bmi < 30) {
        text = 'You are overweight.';
      } else if (bmi < 35) {
        text = 'You are moderately obese.';
      } else if (bmi < 40) {
        text = 'You are severely obese.';
      } else {
        text = 'You are very severely obese.';
      }
      return (
        <View>
          <Text style={styles.modalText}>
            {okay
              ? 'Your bmi is normal for your age.'
              : 'Your bmi is not normal for your age.'}
          </Text>
          <Text style={styles.modalText}>{text}</Text>
        </View>
      );
    }
  };

  useEffect(() => {
    setBmi(Math.round(calculateBMI(height, weight) * 100) / 100 || 0);
  });

  return (
    <>
      <TouchableOpacity onPress={() => setModalVisible(!modalVisible)}>
        <View style={styles.container}>
          <Text style={styles.title}>BMI</Text>
          <Text style={styles.title}>{bmi}</Text>
        </View>
      </TouchableOpacity>
      <Modal animationType='slide' transparent={true} visible={modalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={styles.xContainer}>
              <TouchableOpacity onPress={() => setModalVisible(!modalVisible)}>
                <Text style={styles.closeButon}>×</Text>
              </TouchableOpacity>
            </View>
            <Text style={styles.title}>BMI</Text>
            {renderBMI(bmi, age)}
          </View>
        </View>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 16,
    marginBottom: 16,
    paddingLeft: 16,
    paddingRight: 16,
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    width: '80%',
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  xContainer: {
    width: '100%',
    paddingRight: 8,
    alignItems: 'flex-end',
  },
  closeButon: {
    fontSize: 32,
  },
  modalText: {
    fontSize: 16,
    color: 'grey',
    textAlign: 'center',
    margin: 16,
  },
});

export default BMI;
