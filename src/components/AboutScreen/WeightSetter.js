import React, { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Modal,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { useSelector, useDispatch } from 'react-redux';
import { actions } from '../../redux/ducks';

const WeightSetter = () => {
  const dispatch = useDispatch();

  const storeWeight = useSelector(state => state.weight);
  const weightArray = useSelector(state => state.weightArray);

  const setStoreWeight = weight => dispatch(actions.setWeight(weight));
  const addWeight = weight => dispatch(actions.addWeightToWeightArray(weight));

  const [weight, setWeight] = useState(storeWeight.toString());
  const [modalVisible, setModalVisible] = useState(false);

  const ifAlreadySubmittedToday = () => {
    if (
      weightArray[weightArray.length - 1].date.day === new Date().getDate() &&
      weightArray[weightArray.length - 1].date.month ===
        new Date().getMonth() + 1 &&
      weightArray[weightArray.length - 1].date.year === new Date().getFullYear()
    ) {
      return true;
    } else {
      return false;
    }
  };

  const submitWeight = () => {
    if (ifAlreadySubmittedToday()) {
      ToastAndroid.show(
        'You can update weight once a day!',
        ToastAndroid.SHORT
      );
    } else if (Number.isInteger(Number(weight)) && Number(weight) > 0) {
      setStoreWeight(Number(weight));
      const date = {
        day: new Date().getDate(),
        month: new Date().getMonth() + 1,
        year: new Date().getFullYear(),
      };
      addWeight({
        date,
        weight: Number(weight),
      });
      setModalVisible(!modalVisible);
    } else {
      ToastAndroid.show('Incorrect input!', ToastAndroid.SHORT);
    }
  };

  return (
    <>
      <TouchableOpacity onPress={() => setModalVisible(!modalVisible)}>
        <View style={styles.container}>
          <Text style={styles.title}>Weight</Text>
          <Text style={styles.title}>{storeWeight}</Text>
        </View>
      </TouchableOpacity>
      <Modal animationType='slide' transparent={true} visible={modalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={styles.xContainer}>
              <TouchableOpacity onPress={() => setModalVisible(!modalVisible)}>
                <Text style={styles.closeButon}>×</Text>
              </TouchableOpacity>
            </View>
            <Text style={styles.title}>Weight</Text>
            <TextInput
              keyboardType='numeric'
              placeholder='kg'
              style={styles.input}
              value={weight}
              onChangeText={text => setWeight(text)}
              onSubmitEditing={submitWeight}
            />
          </View>
        </View>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 16,
    marginBottom: 16,
    paddingLeft: 16,
    paddingRight: 16,
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
  },
  input: {
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    margin: 24,
    width: '20%',
    fontSize: 20,
    textAlign: 'right',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    width: '80%',
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  xContainer: {
    width: '100%',
    paddingRight: 8,
    alignItems: 'flex-end',
  },
  closeButon: {
    fontSize: 32,
  },
});

export default WeightSetter;
