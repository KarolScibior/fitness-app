import React from 'react';
import { View } from 'react-native';

const Divider = ({ height = 1, color = '#DCDCDC' }) => {
  return <View style={{ backgroundColor: color, height: height }}></View>;
};

export default Divider;
