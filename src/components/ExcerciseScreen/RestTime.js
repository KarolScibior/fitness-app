import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Countdown from 'react-native-countdown-component';
import Colors from '../../constants/Colors';

const RestTime = props => {
  const { nextExcercise } = props;

  return (
    <View style={styles.container}>
      <Text style={styles.header}>REST A SECOND</Text>
      <Ionicons
        style={styles.animation}
        name="ios-bed"
        size={64}
        color={Colors.tintColor}
      />
      <Countdown
        size={32}
        timeToShow={['S']}
        digitStyle={{ backgroundColor: '#fafafa' }}
        until={1}
        onFinish={nextExcercise}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    fontSize: 32,
    marginBottom: 24,
    textAlign: 'center',
  },
  animation: {
    margin: 16,
  },
});

export default RestTime;
