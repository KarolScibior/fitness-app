import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import Colors from '../../constants/Colors';

const RepeatExcercise = props => {
  const { route, currentExcercise, setCurrentExcercise } = props;

  return (
    <View style={styles.container}>
      <Text style={styles.header}>
        {route.params.excercises[currentExcercise - 1].name}
      </Text>
      <Image
        style={styles.gif}
        source={{
          uri:
            'https://media.tenor.com/images/b3c6af49c64c7dd23caca7f0b2185107/tenor.gif',
        }}
      />
      <Text style={styles.repeats}>
        {route.params.excercises[currentExcercise - 1].repeats} REPEATS
      </Text>
      <TouchableOpacity
        style={styles.button}
        onPress={() => setCurrentExcercise(currentExcercise + 1)}
      >
        <Text style={styles.buttonText}>GO NEXT</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    fontSize: 32,
    marginBottom: 24,
    textAlign: 'center',
  },
  animation: {
    margin: 16,
  },
  repeats: {
    fontSize: 32,
    margin: 12,
  },
  button: {
    backgroundColor: Colors.tintColor,
    padding: 12,
    borderRadius: 12,
  },
  buttonText: {
    color: 'white',
    fontSize: 20,
  },
  gif: {
    width: 220,
    height: 167,
  },
});

export default RepeatExcercise;
