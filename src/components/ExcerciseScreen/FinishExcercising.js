import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Colors from '../../constants/Colors';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../../redux/ducks';

const FinishExcercising = props => {
  const { navigation, item } = props;
  const beginnerTraining = useSelector(state => state.beginnerTraining);
  const intermediateTraining = useSelector(state => state.intermediateTraining);
  const advancedTraining = useSelector(state => state.advancedTraining);
  const dispatch = useDispatch();

  const setProgress = () => {
    dispatch(actions.incrementDaysTrained());
    dispatch(actions.setCurrentDay(item));
    if (item.level === 'Beginner') {
      const temp = beginnerTraining;
      temp.days[item.day - 1].completed = true;
      dispatch(actions.setTrainingPlan('beginner', temp));
    } else if (item.level === 'Intermediate') {
      const temp = intermediateTraining;
      temp.days[item.day - 1].completed = true;
      dispatch(actions.setTrainingPlan('intermediate', temp));
    } else {
      const temp = advancedTraining;
      temp.days[item.day - 1].completed = true;
      dispatch(actions.setTrainingPlan('advanced', temp));
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>GOOD JOB</Text>
      <Ionicons
        style={styles.animation}
        name='ios-nutrition'
        size={64}
        color={Colors.tintColor}
      />
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          setProgress();
          navigation.goBack();
        }}
      >
        <Text style={styles.buttonText}>FINISH</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    fontSize: 32,
    marginBottom: 24,
    textAlign: 'center',
  },
  button: {
    backgroundColor: Colors.tintColor,
    padding: 12,
    borderRadius: 12,
  },
  buttonText: {
    color: 'white',
    fontSize: 20,
  },
  animation: {
    margin: 16,
  },
});

export default FinishExcercising;
