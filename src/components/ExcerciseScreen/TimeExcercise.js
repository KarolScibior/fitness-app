import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import Countdown from 'react-native-countdown-component';

const TimeExcercise = props => {
  const { route, currentExcercise, setBreak } = props;

  return (
    <View style={styles.container}>
      <Text style={styles.header}>
        {route.params.excercises[currentExcercise - 1].name}
      </Text>
      <Image
        style={styles.gif}
        source={{
          uri:
            'https://media.tenor.com/images/b3c6af49c64c7dd23caca7f0b2185107/tenor.gif',
        }}
      />
      <Countdown
        size={32}
        timeToShow={['S']}
        digitStyle={{ backgroundColor: '#fafafa' }}
        until={2}
        onFinish={setBreak}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    fontSize: 32,
    marginBottom: 24,
    textAlign: 'center',
  },
  animation: {
    margin: 16,
  },
  gif: {
    width: 220,
    height: 167,
  },
});

export default TimeExcercise;
