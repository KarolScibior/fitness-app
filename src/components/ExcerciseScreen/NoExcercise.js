import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Colors from '../../constants/Colors';

const NoExcercise = props => {
  const { setCurrentExcercise, navigation } = props;

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Are you ready to start?</Text>
      <View style={styles.buttonsContainer}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            setCurrentExcercise(1);
          }}
        >
          <Text style={styles.buttonText}>YES</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonRed}
          onPress={() => navigation.goBack()}
        >
          <Text style={styles.buttonText}>NO</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    fontSize: 32,
    marginBottom: 24,
    textAlign: 'center',
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '80%',
  },
  button: {
    backgroundColor: Colors.tintColor,
    padding: 12,
    borderRadius: 12,
  },
  buttonRed: {
    backgroundColor: Colors.errorBackground,
    paddingTop: 12,
    paddingBottom: 12,
    paddingLeft: 16,
    paddingRight: 16,
    borderRadius: 12,
  },
  buttonText: {
    color: 'white',
    fontSize: 20,
  },
});

export default NoExcercise;
