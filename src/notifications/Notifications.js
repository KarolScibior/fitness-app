import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';

export const askPermissions = async () => {
  const { status } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
  if (status !== 'granted') {
    console.log('status not granted');
  }
};

export const sendNotificationImmediately = async () => {
  let notificationId = await Notifications.presentLocalNotificationAsync({
    title: 'Test title',
    body: 'Test body eoeoeoeoeoe',
  });
  console.log(notificationId);
};

export const scheduleNotification = async () => {
  let notificationId = Notifications.scheduleLocalNotificationAsync(
    {
      title: 'Scheduled title',
      body: 'Scheduled body eoeoeoeeoe',
    },
    {
      repeat: 'minute',
      time: new Date().getTime() + 10000,
    }
  );
  console.log(notificationId);
};

export const cancelAllNotifications = () => {
  Notifications.cancelAllScheduledNotificationsAsync();
};
