import * as Training from '../../constants/Trainings';

export const SET_PLAN = 'SET_PLAN';
export const SET_CURRENT_DAY = 'SET_CURRENT_DAY';
export const SET_HEIGHT = 'SET_HEIGHT';
export const SET_WEIGHT = 'SET_WEIGHT';
export const SET_AGE = 'SET_AGE';
export const ADD_WEIGHT_TO_WEIGHT_ARRAY = 'ADD_WEIGHT_TO_WEIGHT_ARRAY';
export const INCREMENT_DAYS_TRAINED = 'INCREMENT_DAYS_TRAINED';
export const SET_NOTIFICATIONS = 'SET_NOTIFICATIONS';

export const actions = {
  setTrainingPlan: (planType, plan) => ({
    type: SET_PLAN,
    payload: { planType, plan },
  }),
  setCurrentDay: day => ({
    type: SET_CURRENT_DAY,
    payload: day,
  }),
  setHeight: height => ({
    type: SET_HEIGHT,
    payload: height,
  }),
  setWeight: weight => ({
    type: SET_WEIGHT,
    payload: weight,
  }),
  setAge: age => ({
    type: SET_AGE,
    payload: age,
  }),
  addWeightToWeightArray: weight => ({
    type: ADD_WEIGHT_TO_WEIGHT_ARRAY,
    payload: weight,
  }),
  incrementDaysTrained: () => ({
    type: INCREMENT_DAYS_TRAINED,
  }),
  setNotifications: () => ({
    type: SET_NOTIFICATIONS,
  }),
};

const initialState = {
  beginnerTraining: Training.default[0],
  intermediateTraining: Training.default[1],
  advancedTraining: Training.default[2],
  currentDay: {},
  height: 0,
  weight: 0,
  age: 0,
  weightArray: [
    {
      date: {
        day: 1,
        month: 6,
        year: 2020,
      },
      weight: 87,
    },
    {
      date: {
        day: 2,
        month: 6,
        year: 2020,
      },
      weight: 86,
    },
    {
      date: {
        day: 3,
        month: 6,
        year: 2020,
      },
      weight: 85,
    },
  ],
  daysTrained: 0,
  notifications: false,
};

export default rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PLAN:
      if (action.payload.planType === 'beginner') {
        return {
          ...state,
          beginnerTraining: action.payload.plan,
        };
      } else if (action.payload.planType === 'intermediate') {
        return {
          ...state,
          intermediateTraining: action.payload.plan,
        };
      } else {
        return {
          ...state,
          advancedTraining: action.payload.plan,
        };
      }

    case SET_CURRENT_DAY:
      return {
        ...state,
        currentDay: action.payload,
      };

    case SET_HEIGHT:
      return {
        ...state,
        height: action.payload,
      };

    case SET_WEIGHT:
      return {
        ...state,
        weight: action.payload,
      };

    case SET_AGE:
      return {
        ...state,
        age: action.payload,
      };

    case ADD_WEIGHT_TO_WEIGHT_ARRAY:
      let flag = false;
      state.weightArray.forEach(item => {
        if (
          item.date.day === action.payload.date.day &&
          item.date.month === action.payload.date.month &&
          item.date.year === action.payload.date.year
        ) {
          flag = true;
        }
      });
      if (flag) {
        return state;
      } else {
        return {
          ...state,
          weightArray: [...state.weightArray, action.payload],
        };
      }

    case INCREMENT_DAYS_TRAINED:
      return {
        ...state,
        daysTrained: state.daysTrained + 1,
      };

    case SET_NOTIFICATIONS:
      return {
        ...state,
        notifications: !state.notifications,
      };

    default:
      return state;
  }
};
